var path = require('path');

var project_path = function(relative_path) {
    return path.join(__dirname, relative_path);
};

module.exports = {
    mode: 'development',
    devtool: 'eval-source-map',
    entry: project_path('src/main.js'),
    output: {
        path: project_path('/public/scripts'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            include: project_path('src/'),
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                        '@babel/preset-react'
                    ]
                }
            }]
        }]
    }
};
